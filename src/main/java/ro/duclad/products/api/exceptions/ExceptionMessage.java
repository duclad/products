package ro.duclad.products.api.exceptions;

import lombok.Getter;

import java.util.List;

@Getter
public class ExceptionMessage {
    private String message;
    private List<String> messages;

    public ExceptionMessage(String message) {
        this.message = message;
    }

    public ExceptionMessage(List<String> messages) {
        this.messages = messages;
    }
}
