package ro.duclad.products.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.duclad.products.services.ProductsService;
import ro.duclad.products.services.domain.Product;

@RestController
@RequestMapping("products")
public class ProductsApi {

    private final ProductsService productsService;

    @Autowired
    public ProductsApi(ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping("/{productCode}")
    public Product getProductPrice(@PathVariable("productCode") String productCode) {
        return productsService.getProduct(productCode);
    }

}
