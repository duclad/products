package ro.duclad.products.services.exceptions;

public class ProductNotFoundException extends I18NException{

    public ProductNotFoundException(String objectId) {
        super("Exception.notFound.product", new String[]{objectId});
    }
}
