package ro.duclad.products.services.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class I18NException extends RuntimeException {
    private String message;
    private Object[] args;
}