package ro.duclad.products.services.exceptions;

public class ProductNotValidException extends I18NException {

    public ProductNotValidException(String objectId) {
        super("Exception.notValid.product", new String[]{objectId});
    }
}
