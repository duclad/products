package ro.duclad.products.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.duclad.products.repositories.ProductsRepository;
import ro.duclad.products.repositories.domain.ProductEntity;
import ro.duclad.products.services.domain.Product;
import ro.duclad.products.services.exceptions.ProductNotFoundException;
import ro.duclad.products.services.exceptions.ProductNotValidException;

@Service
public class ProductsService {

    private final ProductsRepository productsRepository;

    @Autowired
    public ProductsService(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    public Product getProduct(String productCode) {
        ProductEntity productEntity = productsRepository.findById(productCode)
                .orElseThrow(() -> new ProductNotFoundException(productCode));
        if (!productEntity.isValid() || productEntity.getPrice() == null || productEntity.getPrice().getCurrency() == null || productEntity.getPrice().getCurrency().getExchangeRate() == null) {
            throw new ProductNotValidException(productCode);
        }
        return new Product(productEntity.getId(), productEntity.getDescription(), productEntity.getPrice().getPrice() * productEntity.getPrice().getCurrency().getExchangeRate().getRate());
    }
}
