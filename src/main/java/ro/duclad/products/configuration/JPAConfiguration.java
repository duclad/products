package ro.duclad.products.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "ro.duclad.products.repositories")
public class JPAConfiguration {
}
