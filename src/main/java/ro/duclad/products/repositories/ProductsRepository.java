package ro.duclad.products.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.duclad.products.repositories.domain.ProductEntity;

public interface ProductsRepository extends JpaRepository<ProductEntity, String> {
}
