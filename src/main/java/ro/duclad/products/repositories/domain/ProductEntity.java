package ro.duclad.products.repositories.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "PRODUCTS")
@Data
@EqualsAndHashCode(of = "id")
public class ProductEntity {
    @Id
    @Column(name = "ID")
    private String id;
    @Basic
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic
    @Column(name = "VALID", columnDefinition = "INT(1)")
    private boolean valid;
    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private PriceEntity price;
    @Version
    private Long version;
}
