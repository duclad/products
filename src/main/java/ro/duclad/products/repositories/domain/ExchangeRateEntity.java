package ro.duclad.products.repositories.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "EXCHANGE_RATES")
@Data
@EqualsAndHashCode(of = "id")
public class ExchangeRateEntity {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Basic
    @Column(name = "RATE")
    private double rate;
    @Basic
    @Column(name = "EXCHANGE_DATE")
    private Date exchangeDate;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CURRENCY_CODE")
    private CurrencyEntity currency;
}
