package ro.duclad.products.repositories.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "HISTORICAL_EXCHANGE_RATES")
@Data
@EqualsAndHashCode(of = "id")
public class HistoricalExchangeRateEntity {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Basic
    @Column(name = "RATE")
    private double rate;
    @Basic
    @Column(name = "EXCHANGE_DATE")
    private Date exchangeDate;
    @ManyToOne
    @JoinColumn(name = "CURRENCY_CODE", referencedColumnName = "ID", nullable = false)
    private CurrencyEntity currency;
}
