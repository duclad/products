package ro.duclad.products.repositories.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "CURRENCIES")
@Data
@EqualsAndHashCode(of = "id")
public class CurrencyEntity {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private String id;
    @Basic
    @Column(name = "NAME")
    private String name;
    @OneToOne(mappedBy = "currency", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private ExchangeRateEntity exchangeRate;
}
