package ro.duclad.products.repositories.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "HISTORICAL_PRICES")
@Data
@EqualsAndHashCode(of = "id")
public class HistoricalPriceEntity {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Basic
    @Column(name = "PRICE")
    private double price;
    @Basic
    @Column(name = "PRICE_DATE")
    private Date priceDate;
    @ManyToOne
    @JoinColumn(name = "CURRENCY_CODE", referencedColumnName = "ID", nullable = false)
    private CurrencyEntity currency;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_CODE")
    private ProductEntity product;
}
