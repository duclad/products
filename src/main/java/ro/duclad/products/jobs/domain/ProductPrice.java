package ro.duclad.products.jobs.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Date;

@Data
public class ProductPrice {

    private String productCode;
    private double priceInEur;
    private Date priceDate;
}
