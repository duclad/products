package ro.duclad.products.jobs;

import lombok.SneakyThrows;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.FileUrlResource;
import org.springframework.jdbc.core.RowMapper;
import ro.duclad.products.jobs.domain.ProductPrice;

import javax.sql.DataSource;
import java.io.File;
import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.sql.SQLException;

@Configuration
public class DailyPricesExportJobConfiguration {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public DataSource dataSource;

    @Value("${dailyPricesExportPath}")
    public String exportFile;

    public class ProductPriceRowMapper implements RowMapper<ProductPrice> {

        @Override
        public ProductPrice mapRow(ResultSet rs, int rowNum) throws SQLException {
            ProductPrice user = new ProductPrice();
            user.setProductCode(rs.getString("PRODUCT_CODE"));
            user.setPriceInEur(rs.getDouble("PRICE_IN_EUR"));
            user.setPriceDate(rs.getDate("PRICE_DATE"));
            return user;
        }

    }

    @Bean
    public JdbcCursorItemReader<ProductPrice> reader(){
        JdbcCursorItemReader<ProductPrice> reader = new JdbcCursorItemReader<>();
        reader.setDataSource(dataSource);
        reader.setSql("select P.PRODUCT_CODE, p.PRICE*ER.RATE PRICE_IN_EUR, ER.EXCHANGE_DATE PRICE_DATE\n" +
                " from PRODUCTS PR join PRICES P on PR.ID=P.PRODUCT_CODE join EXCHANGE_RATES ER on P.CURRENCY_CODE = ER.CURRENCY_CODE\n" +
                " where PR.VALID=1 order by  P.PRODUCT_CODE");
        reader.setRowMapper(new ProductPriceRowMapper());
        return reader;
    }

    @Bean
    public FlatFileItemWriter<ProductPrice> writer() {
        File exportFile = new File(getExportFile());
        FlatFileItemWriter<ProductPrice> writer = new FlatFileItemWriter<>();
        writer.setResource(new FileSystemResource(exportFile));
        writer.setLineAggregator(new DelimitedLineAggregator<>() {{
            setDelimiter(",");
            setFieldExtractor(new BeanWrapperFieldExtractor<>() {{
                setNames(new String[]{"productCode", "priceInEur", "priceDate"});
            }});
        }});

        return writer;
    }

    private Step step1() {
        return stepBuilderFactory.get("step1").<ProductPrice, ProductPrice> chunk(1000)
                .reader(reader())
                .writer(writer())
                .build();
    }

    @Bean
    public Job dailyPricesExportJob() {
        return jobBuilderFactory.get("dailyPricesExportJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }

    private String getExportFile(){
        return exportFile;
    }
}
